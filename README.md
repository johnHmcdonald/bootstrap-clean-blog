# Start Bootstrap - Clean Blog

Clean Blog is a stylish, responsive blog theme for Bootstrap created by Start Bootstrap. This theme features a blog homepage, about page, contact page, and an example post page along with a working PHP contact form.

## Getting Started

To use this theme, choose one of the following options to get started:
* Download the latest release on Start Bootstrap
* Fork this repository on GitHub

## Bugs and Issues

Have a bug or an issue with this theme? Open a new issue here on GitHub or leave a comment on the template overview page at Start Bootstrap.

## Creator

Start Bootstrap was created by and is maintained by **David Miller**, Managing Parter at Iron Summit Media Strategies.

* https://twitter.com/davidmillerskt
* https://github.com/davidtmiller
* [Carpet Cleaning Melbourne](http://www.carpetscleaners.com.au/)

Start Bootstrap is based on the Bootstrap framework created by Mark Otto and Jacob Thorton.

## Copyright and License

Copyright 2013-2015 Iron Summit Media Strategies, LLC. Code released under the Apache 2.0 license.